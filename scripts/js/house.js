//============== Furniture scripts =================
var table = document.getElementsByTagName("table");
function loopTable(y){
  var t;
  for(t=0; t<table.length; t++){
    if(table[t].classList.contains(y)){
      //=" " resets the display
      table[t].style.display = "";
    }else {
      table[t].style.display = "none";
    }
  }
};

function showKitchen(){
  var x = "kitchen";
  loopTable(x);
};

function showBedroom(){
  var x = "bedroom";
  loopTable(x);
};

function showLiving(){
  var x = "kitchen";
  loopTable(x);
};

function showBathroom(){
  var x = "bathroom";
  loopTable(x);
};

function showDining(){
  var x = "dining";
  loopTable(x);
};

function showEntryway(){
  var x = "entryway";
  loopTable(x);
};

function showExterior(){
  var x = "exterior";
  loopTable(x);
};

function showInterior(){
  var x = "interior";
  loopTable(x);
};

function showLiving(){
  var x = "living";
  loopTable(x);
};

function showAll(){
  document.location.reload();
};

//Kitchen Essentials//

function showBaking(){
  var x = "baking";
  loopTable(x);
};

function showCooking(){
  var x = "cooking";
  loopTable(x);
};

function showGadgets(){
  var x = "gadgets";
  loopTable(x);
};

function showMachines(){
  var x = "machines";
  loopTable(x);
};

function showOnCounters(){
  var x = "counters";
  loopTable(x);
};

function showSink(){
  var x = "sink";
  loopTable(x);
};

function showTools(){
  var x = "tools";
  loopTable(x);
};

function showWallmounts(){
  var x = "wallmounts";
  loopTable(x);
};